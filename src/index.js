import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route, Switch} from "react-router-dom";

import Base from './layouts/base/Base';
import Home from './pages/home/Home';
import Done from './pages/done/Done';
import Signin from './pages/signin/Signin';
import Signup from './pages/signup/Signup';
import PlainLayout from "./layouts/plainLayout/PlainLayout";

import {Provider} from 'react-redux';

import store from "./store/store";
import './index.css';

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Switch>
                <Route path={'/signup'} render={()=> (
                    <PlainLayout>
                        <Route  path='/signup' component={Signup}/>
                    </PlainLayout>
                )}/>
                <Route path={'/signin'} render={()=> (
                    <PlainLayout>
                        <Route  path='/signin' component={Signin}/>
                    </PlainLayout>
                )}/>

                <Route path={'/'} render={()=> (
                    <Base>
                        <Route exact path='/' component={Home}/>
                        <Route path='/done' component={Done}/>
                    </Base>
                )}/>

            </Switch>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);
