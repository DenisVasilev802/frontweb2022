import React from 'react';
import './style.css';
import PropTypes from 'prop-types';

export default class Button extends React.Component {
    render() {
        const {value, className, type, disabled} = this.props;
        return (
            <div className={`${className}`} type={type} disabled={disabled} onClick={this.props.onClick}>
                {value}
            </div>
        );
    }
}
Button.propTypes = {
    value: PropTypes.string,
    className: PropTypes.string,
    type: PropTypes.string,
    disabled: PropTypes.bool,
    background: PropTypes.string,
    onClick:function () {}
};
Button.defaultProps = {
    background: ' ',
    value: ' ',
    className: ' ',
    type: 'button',
    disabled: false
};