import React from 'react';
import PropTypes from 'prop-types';
import './style.css';
import Button from "./button/Button";
import {bindActionCreators} from "redux";
import deleteTask from "../../actions/taskList/deleteTask";
import getTaskList from "../../actions/taskList/getTaskList";
import updateStatusTask from "../../actions/taskList/updateStatusTask";
import {connect} from "react-redux";

class Tasks extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            delete: false
        };
    }

    isPencil = () => {
        if (this.props.checkDone !== "checked") {
            return <Button className="pencil" onClick={this.onClick}/>
        }
    };
    onClick = () => {
        if (this.state.delete === false) {
            this.props.updateStatusTask(this.props.id);
        } else {
            this.setState({
                delete: false
            });
        }
    };
    onDelete = () => {
        this.setState({
            delete: true
        });
        this.props.deleteTask(this.props.id);
    };

    render() {
        const {checkDone} = this.props;
        return (
            <label htmlFor={`task_${this.props.id}`}>
                <article className="article">
                    <input className="check-box__box"
                           type="checkbox"
                           name="task"
                           id={`task_${this.props.id}`}
                           checked={checkDone}
                           onClick={this.onClick}
                    />
                    <div className="check-box__box"/>
                    <Button className="trash" onClick={this.onDelete}
                    />
                    {this.isPencil()}
                    <div className="article__description">
                        {this.props.description}
                    </div>
                </article>
            </label>
        );
    }
}

Tasks.propTypes = {
    checkDone: PropTypes.string,
    id: PropTypes.string,
    description: PropTypes.string,
    deleteTask: function () {},
    updateStatusTask: function () {}
};

Tasks.defaultProps = {
    id: '',
    description: ''
};
const mapStateToProps = (state) => {
    return {
        taskList: state.taskListReducer.taskList,
        sortType: state.taskListReducer.sortType
    }
};

const mapDispatchToProps = (dispatch) => ({
    deleteTask: bindActionCreators(deleteTask, dispatch),
    getTaskList: bindActionCreators(getTaskList, dispatch),
    updateStatusTask: bindActionCreators(updateStatusTask, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Tasks);
