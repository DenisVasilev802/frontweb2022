import * as types from '../actions/taskList/actionTypes'

const initialState = {
    taskList: [],
    todoNumber: 0,
    doneNumber: 0,
    sortType: "DESC",
    temprId: '',
    error: null
};

export default (state = initialState, action) => {
    switch (action.type) {
        case types.SORT_TASK_LIST_SUCCESS: {
            return {

                ...state,
                sortType: action.sortType,
                error: null
            }

        }
        case types.GET_TASK_LIST_SUCCESS: {
            return {
                ...state,

                taskList: action.taskList,
                error: null
            }
        }
        case types.SET_TASK_QUANTITY_SUCCESS: {
            return {
                ...state,
                todoNumber: action.todoNumber,
                doneNumber: action.doneNumber,
                error: null
            }
        }

        case types.GET_TASK_LIST_ERROR: {
            return {
                ...state,
                error: action.error
            }
        }
        case types.SET_TASK_LIST_SUCCESS: {
            return {
                ...state,
                taskList: [action.taskList,
                    ...state.taskList

                ],
                error: null
            }
        }
        case types.SET_TASK_LIST_ERROR: {
            return {
                error: action.error
            }
        }
        case types.UPDATE_TASK_LIST_SUCCESS: {
            return {
                ...state,
                taskList:
                    state.taskList.map((task) => {
                        if(task.id ===action.id) {task.status='done';return task}else {return  task}}),
                error: null
            }
        }
        case types.UPDATE_TASK_LIST_ERROR: {
            return {
                error: action.error
            }
        }
        case types.DELETE_TASK_LIST_SUCCESS: {
            return {
                ...state,
                taskList:
                    state.taskList.map((task) => {
                        if(task.id ===action.id) {task.status='deleted';return task}else {return  task}}),
                error: null
                }

            }
        case types.DELETE_TASK_LIST_ERROR: {
            return {
                error: action.error
            }
        }
        default: {
            return state;
        }

    }
}
/*

taskList:
    state.taskList.map((task, id) => {
        return id === action.id ? null:task)}*/
