import * as types from '../actions/user/actionTypes'


const initialState = {
    authorized: !!localStorage.getItem("jwt"),
    error: null,
    successRegistration: false,
    successAuthorization:false

};

export default (state = initialState, action) => {

    switch (action.type) {
        case types.AUTHORIZE_SUCCESS: {
            return {

                ...state,
                authorized: true,
                successAuthorization: true,
                successRegistration: true,
                error: null
            }

        }
        case types.WHOAMI_SUCCESS: {
            return {
                ...state,
                username:action.username,
                error: null
            }

        }
        case types.AUTHORIZE_FAIL: {
            return {

                ...state,
                successAuthorization: false,
                authorized: false,
                error: null
            }
        }
        case types.REGISTRATION_SUCCESS: {
            return {
                ...state,
                successRegistration: true,
                successAuthorization: true,
                error: null
            }

        }
        case types.REGISTRATION_FAIL: {
            return {

                ...state,
                successRegistration: false,
                error: null
            }
        }
        default: {
            return state;
        }

    }
}