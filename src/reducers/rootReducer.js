import {combineReducers} from "redux";
import taskListReducer from "./taskListReducer";
import userListReducer from "./userListReducer";
import {
    i18nReducer
} from 'react-redux-i18n';

export default (state = {}, action) => {
    return combineReducers({
        taskListReducer,
        userListReducer,
        i18n: i18nReducer

    })(state, action);
}