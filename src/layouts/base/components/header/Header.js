import React from 'react';
import logo from './images/logo.svg';
import './style.css';
import {connect} from "react-redux";
import PropTypes from 'prop-types';
 class Header extends React.Component {


     render() {
        return (
            <header className='header'>
                <div className='header__content'>
                    <a href={"/"}>
                        <img src={logo} className="./logo.swg" alt="logo"/>
                    </a>
                    <p className='header__text'>{this.props.username}</p>
                </div>
            </header>
        );
    }
}
Header.propTypes = {
    username: PropTypes.string
};
const mapStateToProps = (state) => {
    return {
        username: state.userListReducer.username

    }
};

const mapDispatchToProps = () => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
