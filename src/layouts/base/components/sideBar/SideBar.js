import React from 'react';
import PropTypes from 'prop-types';
import {NavLink} from "react-router-dom";

import './style.css';
import {connect} from "react-redux";
import { I18n } from 'react-redux-i18n';
class SideBar extends React.Component {
    render() {
        const {className} = this.props;

        return (
            <aside className={`side-bar${className ? ` ${className}` : ''}`}>
                <ul className='side-bar__list'>
                    <li className={"side-bar__item"}>
                        <NavLink className="side-bar__link"
                                 exact
                                 to={"/"}
                                 activeClassName="side-bar__link_active">
                            <div className="side-bar__pic side-bar__pic_do"></div>
                            {I18n.t('sidebar.todo')} ({this.props.todoNumber})
                        </NavLink>

                    </li>
                    <li className={"side-bar__item"}>
                        <NavLink className="side-bar__link"
                                 to={"/done"}
                                 activeClassName="side-bar__link_active">
                            <div className="side-bar__pic side-bar__pic_done"></div>
                            {I18n.t('sidebar.done')} ({this.props.doneNumber})
                        </NavLink>

                    </li>
                </ul>
            </aside>
        );
    }
}

SideBar.propTypes = {
    className: PropTypes.string,
    doneNumber:PropTypes.string,
    todoNumber:PropTypes.string
};

SideBar.defaultProps = {
    className: ''
};
const mapStateToProps = (state) => {
    return {
        todoNumber:state.taskListReducer.todoNumber,
        doneNumber:state.taskListReducer.doneNumber


    }
};
const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);