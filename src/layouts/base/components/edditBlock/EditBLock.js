import React from 'react';
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux';
import './style.css';
import FormField from "./formField/FormField";
import Button from "./formField/button/Button";
import SetTask from "../../../../actions/taskList/setTask"
import SortBlock from "./sort/SortBlock";
import { I18n } from 'react-redux-i18n';

class EditBLock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: ''
        };
    }

    onChange = (event) => {

        this.setState({
            value: event.target.value
        });
    };
    onSubmit = (event) => {
        event.preventDefault();
        this.setState({
            value: '',
        });
        this.props.setTask(this.state.value);
        this.props.updateData();

    };



    render() {
        return (
            <div className='edit_block'>
                <div className='edit_block__content'>
                    <form
                        className="form"
                        onSubmit={this.onSubmit}>
                        <FormField

                            value={this.state.value}
                            className="edit__input"
                            placeholder={I18n.t('editBlock.placeHolder')}
                            onChange={this.onChange}/>
                        <Button
                            className="button button_create"
                            type="submit"
                            disabled={this.state.value.length === 0}
                            value={I18n.t('editBlock.buttonCreateText')}/>
                    </form>

                    <button className='button_upload button'>
                        {I18n.t('editBlock.buttonUpload')}

                    </button>
                    <SortBlock/>
                </div>
            </div>
        );
    }
}
EditBLock.propTypes = {
    setTask: function () {},
    updateData:function () {}
};
const mapStateToProps = (state) => {
    return {
        taskList: [
            state.taskListReducer.taskList
        ]
    }
};

const mapDispatchToProps = (dispatch) => ({
    setTask: bindActionCreators(SetTask, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(EditBLock);


