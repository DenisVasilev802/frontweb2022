import React from 'react';
import './style.css';
import PropTypes from 'prop-types';

export default class Button extends React.Component {
    render() {
        const {value, className, type, disabled} = this.props;
        return (
            <button className={`button ${className}`} type={type} disabled={disabled}>
                {value}
            </button>
        );
    }
}
Button.propTypes = {
    value: PropTypes.string,
    className: PropTypes.string,
    type: PropTypes.string,
    disabled: PropTypes.bool
};
Button.defaultProps = {

    value: ' ',
    className: ' ',
    type: 'button',
    disabled: false
};