import React from 'react';
import './style.css';
import {bindActionCreators} from "redux";
import sortTask from "../../../../../actions/taskList/sortTask";
import {connect} from "react-redux";
import getTaskList from  "../../../../../actions/taskList/getTaskList";
import { I18n } from 'react-redux-i18n';
import PropTypes from 'prop-types';
 class SortBlock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sort: "down"
        };
    }
    changeSort=()=>{
        if(this.state.sort==="up"){
            this.setState({
                sort:"down"
            });
            this.props.sortTask("DESC");
        }
        else {
            this.setState({
                sort:"up"
            });
            this.props.sortTask("ASC");
        }
        this.timer = setTimeout(() => this.props.getTaskList(this.props.sortType), 100);
    };
    render() {
        return (
            <label className={`sort`} onClick={this.changeSort}>
                <div className={`arrow ${this.state.sort}`}></div>
                <p className='sort__text'>{I18n.t('editBlock.sort')}</p>
            </label>
        );
    }

}
SortBlock.propTypes = {
    sortTask: function () {},
    getTaskList: function () {},
    sortType: PropTypes.string
};

const mapStateToProps = (state) => {
    return {
        taskList: state.taskListReducer.taskList,
        sortType:state.taskListReducer.sortType,
    }
};

const mapDispatchToProps = (dispatch) => ({
    sortTask: bindActionCreators(sortTask, dispatch),
    getTaskList: bindActionCreators(getTaskList, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(SortBlock);