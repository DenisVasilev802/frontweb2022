import React from 'react';
import Footer from "../../layouts/base/components/footer/Footer";
import './style.css';
import PropTypes from 'prop-types';

export default class PlainLayout extends React.Component {
    render() {
        return (
            <React.Fragment>
                <main className='plain'>
                    <section className='plain__content'>
                        {this.props.children}
                    </section>
                    <Footer/>
                </main>
            </React.Fragment>
        );
    }
}

PlainLayout.propTypes = {
    children: PropTypes.node.isRequired
};
