import React from 'react';
import './style.css';
import FormField from "../../../base/components/edditBlock/formField/FormField";
import Button from "../../../base/components/edditBlock/formField/button/Button";
import {bindActionCreators} from "redux";
import signIn from "../../../../actions/user/signin";
import signUp from "../../../../actions/user/signup";
import {connect} from "react-redux";
import { I18n } from 'react-redux-i18n';
import PropTypes from 'prop-types';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            password: '',
            agree: true,
            colorAuthorisation:true,
            formFieldClassName:"edit__input",
            loginField:'login__field'
        };
    }

    onChange = (event) => {
        this.setState({
            value: event.target.value,
            formFieldClassName: "edit__input",
            loginField:'login__field'
        });
    };
    onChangeAgree = () => {
        this.setState({
            agree:  true
        });
    };
    onChangePassword = (event) => {
        this.setState({
            password: event.target.value,
            formFieldClassName: "edit__input",
            loginField:'login__field'
        });
    };
    onSubmit = (event) => {

        event.preventDefault();
        if (this.props.agreeToSharePersonalData === true) {
            this.props.signUp(this.state.value, this.state.password);
        } else {
            this.props.signIn(this.state.value, this.state.password);
        }
        this.timer = setTimeout(() => this.setErrorClassName(), 100);
    };
    setDisableAgree = () => {
        if (this.state.value.length === 0 || this.state.password.length === 0) {
            return "check-box__login check-box__login_disable"
        } else return "check-box__login";
    }
    agreeToShareData = () => {
        if (this.props.agreeToSharePersonalData === true) {

            return <label htmlFor='agree'>
                <p className={'login__data_agree'}>
                    <input className="check-box__login"
                           disabled={this.state.value.length === 0 || this.state.password.length === 0}
                           type="checkbox"
                           id='agree'
                           onChange={this.onChangeAgree}
                           name="agreePersonalData"/>
                    <div className={this.setDisableAgree()}
                    />
                    {I18n.t('signUp.agreePersonalData')}
                </p>
            </label>
        }
    }
    setErrorClassName=()=> {
        if (this.props.successAuthorization === false || this.props.successRegistration===false){
            this.setState({
                formFieldClassName: "edit__input edit__error",
                loginField:'login__field login__field_error'
            });
    }    }

    render() {
        return (
            <label className='login'>
                <div className='login__content'>
                    <div className='login__logo'/>
                    <form onSubmit={this.onSubmit}>

                        <div className={this.state.loginField}>
                            <FormField
                                value={this.state.value}
                                className={this.state.formFieldClassName}
                                placeholder={I18n.t('signUp.placeHolder')}
                                onChange={this.onChange}/>
                        </div>
                        <div className="login__field_password">
                            <FormField
                                value={this.state.password}
                                className={this.state.formFieldClassName}
                                placeholder={I18n.t('signUp.placeHolderPassword')}
                                type="password"
                                onChange={this.onChangePassword}/>
                        </div>
                        {this.agreeToShareData()}
                        <Button
                            className="button button_login"
                            type="submit"
                            disabled={this.state.value.length === 0 || this.state.password.length === 0 ||(
                                document.getElementById('agree')!==null &&
                            document.getElementById('agree').checked === false)}
                            value={this.props.buttonText}/>
                        <p className="login__text">
                            {this.props.informText}
                        </p>
                    </form>
                    <p className="login__link">
                        <a className="login__link_links" href={this.props.linkToNewPage}>
                            {this.props.linkText}
                        </a>
                    </p>


                </div>
            </label>
        );
    }
}
Login.propTypes = {
    agreeToSharePersonalData: PropTypes.bool,
    successAuthorization: PropTypes.bool,
    successRegistration: PropTypes.bool,
    linkText:PropTypes.string,
    linkToNewPage:PropTypes.string,
    buttonText:PropTypes.string,
    informText:PropTypes.string,
    signUp: function () {},
    signIn: function () {}
};
const mapDispatchToProps = (dispatch) => ({
    signIn: bindActionCreators(signIn, dispatch),
    signUp: bindActionCreators(signUp, dispatch)
});
const mapStateToProps = (state) => ({
    successAuthorization: state.userListReducer.successAuthorization,
    successRegistration: state.userListReducer.successRegistration
});
export default connect(mapStateToProps, mapDispatchToProps)(Login);

