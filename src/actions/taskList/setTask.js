import {set} from '../../fetcher/fetcher';
import * as types from './actionTypes';

export default function setTaskList(text) {
    return (dispatch) => {
        return set('http://localhost:8080/tasks', text)
            .then(response => {
                dispatch({
                    type: types.SET_TASK_LIST_SUCCESS,
                    taskList: response

                });
            })
            .catch(error => {
                dispatch({
                    type: types.SET_TASK_LIST_ERROR,
                    error: error
                });
            })
    }

}