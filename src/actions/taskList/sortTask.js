import {get} from '../../fetcher/fetcher';
import * as types from './actionTypes';

export default function sortTaskList(sort) {
    return (dispatch) => {
        return get('http://localhost:8080/tasks/?order=' + sort)
            .then(
                dispatch({
                    type: types.SORT_TASK_LIST_SUCCESS,
                    sortType: sort
                }))
            .catch(error => {
                dispatch({
                    type: types.SORT_TASK_LIST_ERROR,
                    error: error
                });
            })
    }
}
