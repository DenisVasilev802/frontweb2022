import {get} from '../../fetcher/fetcher';
import  * as types from './actionTypes';
export default  function getTaskList(sort) {
    return(dispatch)=>{
        return get('http://localhost:8080/tasks/?order='+sort)
            .then(response=>{
                dispatch({
                type:types.GET_TASK_LIST_SUCCESS,
                    taskList:response.tasks

            });})
        .catch(error=>{
            dispatch({
                type:types.GET_TASK_LIST_ERROR,
                error :error
            });
        })
    }

}
