import {update} from "../../fetcher/fetcher";
import * as types from "./actionTypes";

export default function updateTaskList(id) {

    return (dispatch) => {
        return update("http://localhost:8080/tasks/" + id)
            .then(
                dispatch({
                    type: types.UPDATE_TASK_LIST_SUCCESS,
                    id: id
                }))
            .catch(error => {
                dispatch({
                    type: types.UPDATE_TASK_LIST_ERROR,
                    error: error
                });
            })
    }

}