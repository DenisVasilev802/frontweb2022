import * as types from './actionTypes';

export default function setTaskList(quantityDone, quantityInbox) {
    return (dispatch) => {
        dispatch({
            type: types.SET_TASK_QUANTITY_SUCCESS,
            todoNumber: quantityInbox,
            doneNumber: quantityDone

        });
    }
}