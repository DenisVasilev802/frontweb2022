import {del} from '../../fetcher/fetcher';
import * as types from './actionTypes';

export default function deleteTask(id) {

    return (dispatch) => {
        return del("http://localhost:8080/tasks/" + id)
            .then(
                dispatch({
                    type: types.DELETE_TASK_LIST_SUCCESS,
                    id: id
                }))
            .catch(error => {
                dispatch({
                    type: types.DELETE_TASK_LIST_ERROR,
                    error: error
                });
            })
    }

}