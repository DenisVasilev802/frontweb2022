import {signin} from '../../fetcher/fetcher';
import * as types from './actionTypes';

export default function sign(username,password) {

    return (dispatch) => {
        return signin('http://localhost:8080/signin',username,password)
            .then(response => {
                localStorage.setItem('jwt',response.token);
                dispatch({
                    type: types.AUTHORIZE_SUCCESS
                });

            })
            .catch(error => {
                dispatch({
                    type: types.AUTHORIZE_FAIL,
                    error: error
                });
            })
    }


}