import {signup} from '../../fetcher/fetcher';
import * as types from './actionTypes';

export default function sign(username,password) {

    return (dispatch) => {
        return signup('http://localhost:8080/signup',username,password)
            // eslint-disable-next-line no-unused-vars
            .then(response => {
                dispatch({

                    type: types.REGISTRATION_SUCCESS
                });

            })
            .catch(error => {
                dispatch({
                    type: types.REGISTRATION_FAIL,
                    error: error
                });
            })
    }


}
