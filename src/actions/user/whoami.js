
import {get} from '../../fetcher/fetcher';
import  * as types from './actionTypes';
export default  function whoAmI() {
    return(dispatch)=>{
        return get('http://localhost:8080/whoami')
            .then(response=>{
                // eslint-disable-next-line no-console
                console.log(response)
                dispatch({
                    type:types.WHOAMI_SUCCESS,
                    username:response.username

                });})
            .catch(error=>{
                localStorage.removeItem('jwt');
                dispatch({
                    type:types.GET_TASK_LIST_ERROR,
                    error :error
                });
            })
    }

}
