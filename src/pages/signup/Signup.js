import React from 'react';


import Login from "../../layouts/plainLayout/components/loginForm/Login";
import {bindActionCreators} from "redux";
import signIn from "../../actions/user/signin";
import {connect} from "react-redux";
import {I18n} from 'react-redux-i18n';
import PropTypes from 'prop-types';

class Signin extends React.Component {
    componentDidMount() {
        if (this.props.authorized) {
            this.props.history.push('/');
        }
    }

    componentDidUpdate() {
        if (this.props.authorized) {
            this.props.history.replace('/');
        }
        if (this.props.successRegistration === true) {
            this.props.history.push('/signin');
        }

    }

    constructor(props) {
        super(props);
        this.buttonText = I18n.t('signUp.ButtonSubmit');
        this.informText = I18n.t('signUp.informText');
        this.linkText = I18n.t('signUp.linkText');
        this.linkToNewPage = "/signin"
        this.agreeToSharePersonalData = true
    }

    render() {

        return (
            <React.Fragment>
                <Login buttonText={this.buttonText} informText={this.informText} linkText={this.linkText}
                       linkToNewPage={this.linkToNewPage}
                       agreeToSharePersonalData={this.agreeToSharePersonalData}/>
            </React.Fragment>
        );
    }
}

Signin.propTypes = {
    authorized: PropTypes.bool,
    successRegistration: PropTypes.bool,
    history:PropTypes
};
const mapDispatchToProps = (dispatch) => ({
    signIn: bindActionCreators(signIn, dispatch)
});
const mapStateToProps = (state) => ({
    authorized: state.userListReducer.authorized,
    successRegistration: state.userListReducer.successRegistration
});
export default connect(mapStateToProps, mapDispatchToProps)(Signin);