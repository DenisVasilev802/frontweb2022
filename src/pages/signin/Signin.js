import React from 'react';


import Login from "../../layouts/plainLayout/components/loginForm/Login";
import {bindActionCreators} from "redux";
import signIn from "../../actions/user/signin";
import {connect} from "react-redux";
import {I18n} from "react-redux-i18n";
import PropTypes from 'prop-types';
 class Signin extends React.Component {
     componentDidMount() {
         if (this.props.authorized) {
             this.props.history.push('/');
         }
     }
     constructor(props) {
         super(props);
         this.buttonText=I18n.t('signIn.ButtonSubmit');
         this.informText=I18n.t('signIn.informText');
         this.linkText=I18n.t('signIn.linkText');
         this.linkToNewPage="/signup"
     }
     componentDidUpdate() {
         if (this.props.authorized) {
             this.props.history.replace('/');
         }

     }
    render() {

        return (
            <React.Fragment>
                <Login buttonText={this.buttonText} informText={this.informText} linkText={this.linkText} linkToNewPage={this.linkToNewPage} />
            </React.Fragment>
        );
    }
}
Signin.propTypes = {
    authorized: PropTypes.bool,
    history:PropTypes.history
};
const mapDispatchToProps = (dispatch) => ({
    signIn: bindActionCreators(signIn, dispatch)
});
const mapStateToProps = (state) => ({
    authorized: state.userListReducer.authorized
});
export default connect(mapStateToProps, mapDispatchToProps)(Signin);