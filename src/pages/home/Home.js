import React from 'react';

import Tasks from '../../components/tasks/Tasks';
import {connect} from 'react-redux';
import EditBlock from "../../layouts/base/components/edditBlock/EditBLock";
import {bindActionCreators} from "redux";
import getTaskList from "../../actions/taskList/getTaskList";
import setQuantity from "../../actions/taskList/setQuantityTask";
import whoami from "../../actions/user/whoami";
import PropTypes from 'prop-types';
class Home extends React.Component {
    CountQuantity(){
        this.props.taskList.map((task) => {
            if(task.status==="inbox"){
                this.setState({
                    quantityInbox: this.state.quantityInbox+1
                });
            }
            if(task.status==="done"){
                this.setState({
                    quantityDone: this.state.quantityDone+1
                });
            }
        });
        this.props.setQuantity(this.state.quantityDone,this.state.quantityInbox);
    }
    componentDidMount() {
        if (!this.props.authorized) {
            this.props.history.replace('/signin');
        }
        this.props.whoami();
        this.props.getTaskList(this.props.sortType);
        this.timer = setTimeout(() => this.CountQuantity(), 100);
    }



    constructor(props) {
        super(props);
        this.state = {
            quantityDone:0,
            quantityInbox:0
        };

    }

    isDone(task, text) {
        if (task.status !== "done" && task.status!=='deleted') {
            return <Tasks
                key={text} id={task.id} description={task.text}/>
        } else return null;
    }

    renderList = () => {
        return this.props.taskList.map((task, text) => {
                return (
                    this.isDone(task, text)
                );
        });
    };
    updateData = () => {
      //  this.timer = setTimeout(() => this.props.getTaskList(this.props.sortType), 100);
    };

    render() {
        return (
            <React.Fragment>
                <EditBlock className='main__edit_block'
                           updateData={this.updateData}/>
                {this.renderList()}
            </React.Fragment>
        );
    }
}
Home.propTypes = {
    authorized: PropTypes.bool,
    taskList: PropTypes.list,
    sortType: PropTypes.string,
    getTaskList: function () {},
    whoami:function(){},
    setQuantity: function () {},
    history:PropTypes.history
};
const mapDispatchToProps = (dispatch) => ({
    getTaskList: bindActionCreators(getTaskList, dispatch),
    whoami: bindActionCreators(whoami,dispatch),
    setQuantity: bindActionCreators(setQuantity,dispatch)
});
const mapStateToProps = (state) => ({
    taskList: state.taskListReducer.taskList,
    sortType: state.taskListReducer.sortType,
    authorized: state.userListReducer.authorized

});
export default connect(mapStateToProps, mapDispatchToProps)(Home);
