function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    }
    throw new Error(response.statusText);

}



export function get(url) {
    const token = localStorage.getItem('jwt');

    return fetch(url, {
        method: 'GET',
        headers: new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`


        })

    })
        .then((response) => checkStatus(response))
        .then((response) => response.json())
        .catch((error) => {
            throw error;
        })

}

export function set(url, text) {
    const token = localStorage.getItem('jwt');

    return fetch(url, {
        method: 'POST',
        headers: new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }),
        body: JSON.stringify({
            text: text

        })


    })
        .then((response) => checkStatus(response))
        .then((response) => response.json())
        .catch((error) => {
            throw error;
        })

}

export function del(url) {
    const token = localStorage.getItem('jwt');

    return fetch(url, {
        method: 'DELETE',
        headers: new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }),

    })
        .then((response) => checkStatus(response))
        .catch((error) => {
            throw error;
        })

}

export function update(url) {
    const token = localStorage.getItem('jwt');

    return fetch(url, {
        method: 'PATCH',
        headers: new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }),
        body: JSON.stringify({
            status: "done"

        })

    })
        .then((response) => checkStatus(response))
        .catch((error) => {
            throw error;
        })

}
export function signin(url, username,password) {
    return fetch(url, {
        method: 'POST',
        headers: new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }),
        body: JSON.stringify({
            username: username,
            password: password

        })


    })
        .then((response) => checkStatus(response))
        .then((response) => response.json())
        .catch((error) => {
            throw error;
        })

}
export function signup(url, username,password) {
    return fetch(url, {
        method: 'POST',
        headers: new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }),
        body: JSON.stringify({
            username: username,
            password: password

        })


    })
        .then((response) => checkStatus(response))
        .catch((error) => {
            throw error;
        })

}
